from django.conf.urls import url, patterns

from . import views

urlpatterns = patterns('',
    url(r'^home/$', views.index, name='home'),
    url(r'^$', views.FeedsList.as_view(), name='createfeed'),
    url(r'^self/$', views.MyFeedsList.as_view(), name='myfeeds'),
    url(r'^details/(?P<pk>[0-9]+)$', views.FeedsDetail.as_view() ),
    )
