from rest_framework import serializers
from feeds.models import NewsFeed

class FeedsSerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsFeed
        # fields = ('id', 'headline', 'content', 'pub_date',
        #            'image','user_id','agrees_count','disagrees_count','long_position','lat_position' )
