from django.shortcuts import render
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.db.models import Q
from django.http.response import Http404
from django.shortcuts import render, get_object_or_404, render_to_response

from feeds.serializers import FeedsSerializer
from feeds.models import NewsFeed
from newsknit.functions import ResponseStatus

from rest_framework import generics, status
from rest_framework.views import APIView

def index (request):
    current_user = request.user
    print current_user.id
    return HttpResponse("Hello, world. You're at the news knit feeds index.")

class FeedsAPIView(APIView):
    def get_object(self, pk):
        try:
            return NewsFeed.objects.get( pk = pk )
        except NewsFeed.DoesNotExist:
            return ResponseStatus( '', status.HTTP_400_BAD_REQUEST )
        

class FeedsList(FeedsAPIView):
        
    def get(self, request, format=None):
        paginate_by = 20
        feeds = NewsFeed.objects.filter( ~Q( user_id = request.user.id ) )        
        serializer = FeedsSerializer(feeds, many=True)
        return ResponseStatus(serializer.data, 200)

    def post(self, request, format=None):
        request.data['user'] = request.user.id
        serializer = FeedsSerializer( data=request.data )
        if serializer.is_valid():
            serializer.save()
            return ResponseStatus(serializer.data, status=status.HTTP_201_CREATED)
        return ResponseStatus(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    
class FeedsDetail(FeedsAPIView):

    def patch(self, request, pk, format=None):
        feed = self.get_object(pk)
        serializer = FeedsSerializer( feed, data = request.data, partial=True )
        if serializer.is_valid():
            serializer.save()            
            return ResponseStatus(serializer.data, 200)
        return ResponseStatus( serializer.errors, status.HTTP_400_BAD_REQUEST )        
    
# My Feeds
class MyFeedsList(FeedsList):
    
    def get(self, request, format=None):
        paginate_by = 20
        feeds = NewsFeed.objects.filter( user_id = request.user.id )
        serializer = FeedsSerializer(feeds, many=True)
        return ResponseStatus(serializer.data, 200)
    