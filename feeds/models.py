from django.db import models
from django.utils import timezone
from django.conf import settings

# Create your models here.
    
class NewsFeed(models.Model):
    
    id = models.AutoField(primary_key=True)
    headline = models.CharField(max_length=255, null=True, blank=True)
    content = models.CharField(max_length=1000, null=True, blank=True)
    pub_date = models.DateTimeField(null=True, blank=True)
    image = models.FileField(upload_to='candidate-photos', null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    agrees_count = models.IntegerField(default=0)
    disagrees_count = models.IntegerField(default=0)
    
    long_position = models.DecimalField (max_digits=8, decimal_places=3, null=False, blank=False)
    lat_position = models.DecimalField (max_digits=8, decimal_places=3, null=False, blank=False)
    
    created = models.DateTimeField(null=True, blank=True, editable=False)
    modified = models.DateTimeField(null=True, blank=True)    
