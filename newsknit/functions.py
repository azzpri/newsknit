from rest_framework.response import Response
from django.http.response import HttpResponse

import json

def ResponseStatus( data, status, *args, **kwargs ):
    
    data = {
        'status' : status,
        'data' : data,        
    }
    
    return Response(  data, *args, **kwargs )

def HttpResponseStatus( data, status, *args, **kwargs ):
    
    data = {
        'status' : status,
        'data' : data,        
    }
    
    return HttpResponse(  json.dumps(data) , *args, **kwargs )
