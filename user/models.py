from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.conf import settings
from rest_framework.authtoken.models import Token

# Create your models here.
class Profile(models.Model):
    
    id          = models.AutoField(primary_key=True)
    user        = models.ForeignKey(settings.AUTH_USER_MODEL)
    age         = models.IntegerField(default=0)
    gender      = models.CharField(max_length=20)
    lat         = models.FloatField(null=True,blank=True)
    lon         = models.FloatField(null=True,blank=True)
    phone       = models.CharField(max_length=45,null=True,blank=True)
    followers   = models.ManyToManyField('Profile')
    created     = models.DateTimeField(null=True,blank=True,editable=False)
    modified    = models.DateTimeField(null=True,blank=True,)
                                                          
    def __unicode__(self):
        return unicode(self.user)
    
    
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)