from django.shortcuts import render
from django.http.response import HttpResponse
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.http.response import Http404
from django.shortcuts import render, get_object_or_404, render_to_response
from django.contrib.auth import authenticate, login, logout

from user.serializers import ProfileSerializer,UserSerializer,AuthTokenSerializer
from user.models import Profile

from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes

from newsknit.functions import ResponseStatus, HttpResponseStatus

import json, pdb

def index (request):
    return HttpResponse("Hello, world. You're at the news knit profiles index.")

# PROFILE
class ProfileMixin(object):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()
    paginate_by = 20
    
    def get_queryset(self):
        '''
        Optionally restricts the returned purchases to a given user,
        '''
        user = self.request.user
        queryset = Profile.objects.filter(user=user)
        return queryset
    
class ProfileList(ProfileMixin, generics.ListAPIView):
    pass
    
    def create(self, request, *args, **kwargs):
        return generics.ListAPIView.create(self, request, *args, **kwargs)  

class ProfileDetail(ProfileMixin, generics.RetrieveUpdateAPIView):
    
    def update(self, request, *args, **kwargs):
        request.data['user'] = request.user.id
        return generics.RetrieveUpdateAPIView.update(self, request, *args, **kwargs)

# PROFILES - All available profiles
class ProfilesMixin(object):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()
    paginate_by = 20
    
    def get_queryset(self):
        queryset = Profile.objects.all()
        return queryset
    
class ProfilesList(ProfilesMixin, generics.ListAPIView):
    pass
    
    def create(self, request, *args, **kwargs):
        return generics.ListAPIView.create(self, request, *args, **kwargs) 

@csrf_exempt 
def registerUser(request):
    
    userName = request.POST.get('username')
    userPass = request.POST.get('password')
    userMail = request.POST.get('email')

    try:
        user = User.objects.create_user(username = userName,
                                 email = userMail,
                                 password = userPass)
    except Exception:
        return HttpResponseStatus( { "message": 'username is already exist' }, 500 )
    
    userProfile = Profile.objects.get_or_create( user = user )
    
    token, a = Token.objects.get_or_create( user = user )         
    return HttpResponseStatus( { "token": token.key }, 200 )
    
# LOGIN
@csrf_exempt
def login_auth(request):

        userName = request.POST.get('username')
        userPass = request.POST.get('password')
    
        response_data = {}
        response_data['data'] = ''
        
        user = authenticate(username=userName, password=userPass)
        
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseStatus( {
                        "message": "user is active",
                        'status': 1
                    }, 200 )                
            else:                              
                return HttpResponseStatus( {
                        "message": "user is not active",
                        'status': 2
                    }, 500 )
        else:            
            return HttpResponseStatus( {
                    "message": "user is not available",
                    'status': 0
                }, 500 )

# LOGOUT   
@csrf_exempt
def logout_view(request):
    logout(request)    
    return HttpResponseStatus( { "message": "User is logged out" }, 200 )

# LOGIN-STATUS
@csrf_exempt
def login_status(request):
    if not request.user.is_authenticated():        
        return HttpResponseStatus( {
                "message": "User is authenticated",
                "status": 1
            }, 200 )
    else :
        return HttpResponseStatus( {
                "message": "User is not authenticated",
                "status": 0
            }, 200 )
