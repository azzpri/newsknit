from rest_framework import serializers
from user.models import Profile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _

class ProfileSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Profile

class UserSerializer(serializers.ModelSerializer): 
       
    class Meta:
        model = User

class AuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
            else:
                msg = _('Unable to log in with provided credentials.')
        else:
            msg = _('Must include "username" and "password"')

        attrs['user'] = user
        return attrs
